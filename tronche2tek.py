import base64
import glob
import os
import requests
from face_detection import face_centering
import config

# POST https://tronche2tek.epitest.eu/api/picture/armelle.bengochea@epitech.eu
# Need cookie (.env file ?!)
# JSON body {"original":null,"intra":"data:image/bmp;base64,'BMP encoding'"
# 320w x 240h


def upload(login: str, img_base64: bytes):
    base_url = 'https://tronche2tek.epitest.eu/api/picture/'
    url = f'{base_url}{login}'
    cookies = dict(tronche2tek_oauth2_proxy=config.OAUTH_COOKIE)
    headers = {'Content-Type': 'application/json'}
    img_base64 = img_base64.decode('utf-8')
    data = f'{{"original":null,"intra":"data:image/bmp;base64,{img_base64}"}}'
    r = requests.post(url, data=data, cookies=cookies, headers=headers)
    print(f'Upload done with code {r}')


def encode_base64_image(filepath: str):
    with open(filepath, 'rb') as img:
        data = img.read()
        return base64.b64encode(data)


for filename in glob.glob(os.path.join(config.SRC_DIR, '*.*')):
    login = '.'.join(filename.split('/')[-1].split('.')[:-1])
    print(f'Starting process for {login}...')

    print('Starting face centering...')
    _, filepath = face_centering(filename, config.DEST_DIR, login)
    print('Face centering done.')

    encoded_img = encode_base64_image(filepath)
    # encoded_img = encode_base64_image('Resources/intra_armelle.bengochea@epitech.eu.png')

    print('Starting upload...')
    upload(login, encoded_img)
    print(f'Finished process for {login}')

