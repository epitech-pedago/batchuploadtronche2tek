import face_recognition
from PIL import Image


def face_centering(img_filepath: str, dest_dir: str, student_login: str):

    # Face recognition
    image = face_recognition.load_image_file(img_filepath)
    face_locations = face_recognition.face_locations(image)

    # Face centering
    for face_location in face_locations:
        print('Face found, starting centering, crop, and resize...')

        top, right, bottom, left = face_location

        # face_image = image[top:bottom, left:right]
        # pil_image = Image.fromarray(face_image)
        # pil_image.show()

        height = bottom - top
        width = right - left

        length = width if width > height else height
        length = int(length / 2)

        length_height = int(length * 0.96)  # Percentage is based on ratio expected by tronche2tek -> 32/24 (1.33)
        length_width = int(length * 1.28)

        y_center = top + int(height / 2)
        x_center = left + int(width / 2)

        top = y_center - length_height
        bottom = y_center + length_height
        left = x_center - length_width
        right = x_center + length_width

        top -= length_height * 2
        bottom += length_height * 2
        left -= length_width * 2
        right += length_width * 2

        _, img_height, _ = image.shape
        if top < 0:
            bottom += -top
            top = 0
        elif bottom > img_height:
            top -= bottom - img_height
            bottom = img_height

        # Crop image to proper ratio and centered on the face
        centered_filepath = f'{dest_dir}/centered_{student_login}.png'
        print(f'top: {top}, left: {left}, bottom: {bottom}, right: {right}')
        face_image = image[top:bottom, left:right]
        pil_image = Image.fromarray(face_image)
        pil_image.save(centered_filepath, "PNG")

        # Reduce image to expected size
        intra_filepath = f'{dest_dir}/intra_{student_login}.png'
        pil_image = pil_image.resize((320, 240), Image.ANTIALIAS)
        pil_image.save(intra_filepath, "PNG")

        return centered_filepath, intra_filepath


# face_centering('Resources/oussa.el-manfaloti.JPG', 'Resources', 'oussama.el-manfaloti@epitech.eu')
