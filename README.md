# Batch Upload for Tronche2Tek

This is a python script to simplify the upload of students photos on the intranet.

It does three things:
- Detect face on picture, center/crop/resize to be intra compliant
- Create two photos: centered_login.png and intra_login.png), the latter is the one that will be uploaded to the intranet. The other is the same picture before getting resized, it is at the original photo quality and can be given to students for their CV photo for example.
- Upload the photo on the intranet through tronche2tek.epitest.eu



## Installation

Python 3.7+ is recommended.
Install the package present in requirements.txt with `pip3 install -r requirements.txt`
You might need to install dlib on your OS too.

Fill the `config.py` file with:
- OAUTH_COOKIE -> the session cookie from tronche2tek.epitest.eu
- SRC_DIR -> The directory containing **only** the students photos (**Photos need to be named login.img_format**, example: spam.eggs@epitech.eu.JPG)
- DEST_DIR -> The directory receiving students centered, croped and resized photo

## Execution

The entry file is `tronche2tek.py`

To execute `python3 tronche2tek.py`

It will display progress status for each students.

## Issues
Don't hesite to contact me on MS Teams (alexandra.picot) or create a GitLab issue.

Once the script finished, wait an undertermined amount of time for the intranet to refresh its cache (usually less than 3 hours).
